# FA23 Group 1 Project

# Project Link

[ie421_high_frequency_trading_fall_2023 / ie421_hft_fall_2023_group_01 / group_01_project · GitLab](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2023/ie421_hft_fall_2023_group_01/group_01_project)

# Tech Stack used

Python(Boto3)

AWS services(EC2 and S3)

# Latency Test Automation

Automated the process of installing required packages and fetching testing scripts from S3 before initialization using a user-data script.

And saved all the kernel configurations using an Amazon Machine Image (AMI) to avoid cumbersome shell scripts.

TCP connection Keep-Alive to reduce latency

# Instance Search

## Two Aspects

- the best combination of parameters
- the collocated instances
    - find the zone where Binance exchanges are located
    

# Major Testing Results

> Whole section copied from project’s [README.md](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2023/ie421_hft_fall_2023_group_01/group_01_project/-/blob/main/README.md?ref_type=heads) file
> 

### API Improvement

Without keeping TCP connections alive, the average latency of is 55.8 milliseconds. After implementing TCP connections alive, the average latency is reduced to around 10 milliseconds level. This implementation has the most significant improvement, however, it's not part of the system tuning. It's code optimization. The later tuning will be conducted based on this improvement.

### Availability Zone

This is another easy point to improve our strategy. We ran our latency test scripts for 24 hours on Tokto `zone-a`, `zone-c`and `zone-d` . We got 1.6 milliseconds fatser on `zone-a`. The relative improvement is 16.77%.

### System Tuning

Due to the limit of time, Busy read, busy poll, ban deep c-states and IRQ allocation tuning has not been tested over 24 hours. Thus, the results have larger variance. However, combing all the tunings together we could have around 1.5 millisecons improvement, which is more significant. The relative improvement is 14.89%.

### Collocated Instance Search

We ran the instance search script for 24 hours and each instance was ran for 1 hour. However, we didn't get an instance which has a significant lower latency compared with other random initiated instances. With the results of previous tests that lasted over 24 hours, we found that there was a maximum difference in latency of more than 3 milliseconds at different hours of the day. Thus, we need a much loger test time to eliminate this effect. Since we only have one binance account for testing, we plan to run this script for another 1 month and update the results.