---
- name: Reset OS Level Tuning Parameters
  hosts: all
  become: yes
  tasks:
    # Reset TCP Tuning parameters
    - name: Set TCP_NODELAY back to default
      sysctl:
        name: net.ipv4.tcp_nodelay
        value: "0"
        sysctl_set: yes
        state: present
        reload: yes

    - name: Reset receive and send buffer sizes to default
      sysctl:
        name: "{{ item.name }}"
        value: "{{ item.value }}"
        sysctl_set: yes
        state: present
        reload: yes
      loop:
        - { name: "net.core.rmem_default", value: "212992" }
        - { name: "net.core.wmem_default", value: "212992" }

    - name: Set TCP_QUICKACK back to default
      sysctl:
        name: net.ipv4.tcp_quickack
        value: "1"
        sysctl_set: yes
        state: present
        reload: yes

    - name: Set TCP_FASTOPEN back to default
      sysctl:
        name: net.ipv4.tcp_fastopen
        value: "0"
        sysctl_set: yes
        state: present
        reload: yes

    - name: Disable TCP Keep Alive settings
      sysctl:
        name: net.ipv4.tcp_keepalive_time
        value: "7200" # default is usually 7200 seconds (2 hours)
        sysctl_set: yes
        state: present
        reload: yes

    # CPU Isolation related tasks
    - name: Remove CPU isolation settings
      # This is platform-specific and might need different handling
      # For GRUB-based systems
      lineinfile:
        path: /etc/default/grub
        regexp: "^GRUB_CMDLINE_LINUX_DEFAULT=.*isolcpus=.*"
        line: 'GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"'
        state: present
      notify: update grub

    # IRQ Processing
    - name: Reset IRQ affinity to default
      # Resetting affinity for all IRQs could be disruptive
      # Specify IRQ numbers that you previously set
      command: "echo 1 > /proc/irq/{{ item }}/smp_affinity"
      loop: "{{ irq_list }}"
      when: irq_list is defined

    # Memory settings
    - name: Enable Transparent Huge Pages (THP)
      command: "echo 'always' > /sys/kernel/mm/transparent_hugepage/enabled"

    - name: Set vm.swappiness back to system default
      sysctl:
        name: vm.swappiness
        value: "60"
        sysctl_set: yes
        state: present
        reload: yes

  handlers:
    - name: update grub
      command: update-grub
