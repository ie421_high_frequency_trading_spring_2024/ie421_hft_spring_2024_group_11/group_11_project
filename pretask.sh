set -a  
source ./.env
set +a

# Start the SSH agent in the background
eval "$(ssh-agent -s)"

# Add your SSH private key to the SSH agent
ssh-add ~/.ssh/id_rsa_gh
