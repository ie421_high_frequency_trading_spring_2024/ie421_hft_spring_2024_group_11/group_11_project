# Proposal for AWS Low Latency Tuning Project for High Frequency Trading

## Background

High frequency trading (HFT) has become an integral part of modern financial markets. HFT firms rely on high-speed, low-latency infrastructure to execute trades in milliseconds and gain a competitive edge. Since a lot of financial exchanges are migrated to be hosted on Amazon Web Services (AWS) to take advantage of the scalability, flexibility and cost efficiency of the cloud. Being able to achieve ultra low latency for communication among AWS instances has become a crucial tasks for HFT firms/hedge fund to accomplish. In addition to being useful for reliable and deterministic trading, deterministic and high performance networking has multiple other use cases that are highly relevant to AWS's customer base, including cybersecurity.

However, achieving the extremely low latency required for HFT in the cloud presents challenges. HFT applications are highly sensitive to latency and require extensive performance tuning and optimization. There is a need for more research and testing of latency optimization techniques for HFT workloads running on cloud infrastructure like AWS.

## Proposed Project

We have proposed and are working on an academic research project in IE421(High Frequency Trading) course in University of Illinois Urbana Champaign(UIUC) to investigate varied low-latency optimization techniques specifically for HFT applications running on AWS. The project will focus on:

1. Evaluating the performance of AWS bare metal instances for HFT use cases. Bare metal instances provide direct access to the underlying hardware without virtualization overhead, making them well-suited for latency-sensitive workloads.

2. Testing and comparing various OS, network, application and hardware level tuning techniques to reduce latency of communicating between AWS instances. This includes evaluating approaches like kernel bypass, isolating CPU core, IRQ(Interrupt Request) processing, TCP tuning(disble Naggle algorithm, changing send and receive buffer size), etc.

3. Measuring latency and documenting performance improvements from the optimization techniques tested and generate holistic report on the improvement of every technique. The goal is to produce a set of best practices and reference architectures for deploying low-latency HFT systems on AWS.

4. Open source Ansible Dev-Ops and Terraform IaC provisioning scripts along with detailed results of all of the tuning methods.

5. Publishing the research findings to share knowledge with the wider financial technology and cloud computing community. HFT on the cloud is an area of growing interest and importance.

## Request for AWS Support

To undertake this research project, we are seeking support from AWS in the form of access to AWS resources for testing. Specifically, we are requesting:

- AWS free unlimited plan for the duration of the project to cover usage costs

- Access to bare metal instances, including suitable instance types for HFT testing like m5d.metal, c5d.metal, z1d.metal etc.

- Technical guidance from AWS solution architects and experts in financial services, low-latency applications and EC2 bare metal instances

Access to these resources will allow us to conduct the necessary testing and complete the research project. We believe this project will benefit the broader financial community and cloud computing community by providing valuable insights and guidance on optimizing HFT deployments on AWS. It will also showcase AWS' capabilities in supporting mission-critical, low-latency financial applications.

We would be grateful for AWS' support of this project and the opportunity to work together to advance knowledge in this important area. Please let us know if you have any questions or require further details about the project. We look forward to discussing this opportunity further.
