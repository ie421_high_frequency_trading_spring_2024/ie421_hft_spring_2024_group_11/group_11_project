# Proposal for AWS Low Latency Tuning Project for High Frequency Trading

## Background

High frequency trading (HFT) has become an integral part of modern financial markets. HFT firms rely on high-speed, low-latency infrastructure to execute trades in milliseconds and gain a competitive edge. Since a lot of financial exchanges are migrated to be hosted on Amazon Web Services (AWS) to take advantages of the scalability, flexibility and cost efficiency of the cloud. Being able to achieve ultra low latency for communication among AWS instances has become a crucial tasks for HFT firms/hedge fund to accomplish.

However, achieving the extremely low latency required for HFT in the cloud presents challenges. HFT applications are highly sensitive to latency and accurate timestamp, thus, requiring extensive performance tuning, optimization and time synchronization. There is a need for more researches and testings of latency optimization techniques for HFT workloads running on cloud infrastructure like AWS.

## Proposed Project

We have proposed and are working on two academic research projects in IE421(High Frequency Trading)/IE597 (Independent Study) courses in University of Illinois Urbana Champaign(UIUC).

The first project investigates varied low-latency optimization techniques specifically for HFT applications running on AWS. It focuses on:

1. Evaluating the performance of AWS bare metal instances for HFT use cases. Bare metal instances provide direct access to the underlying hardware without virtualization overhead, making them well-suited for latency-sensitive workloads.
2. Testing and comparing various OS, network, application and hardware level tuning techniques to reduce latency of communicating between AWS instances. This includes evaluating approaches like kernel bypass, isolating CPU core, IRQ(Interrupt Request) processing, TCP tuning(disble Naggle algorithm, changing send and receive buffer size), etc.
3. Measuring latency and documenting performance improvements from the optimization techniques tested and generate holistic report on the improvement of every technique. The goal is to produce a set of best practices and reference architectures for deploying low-latency HFT systems on AWS.


The second project is to implement a low latency and time-accurate ticker plant on AWS. It focuses on:

1. **Data Collection**: subcribes market signals (e.g. order book updates) using the WebSocket connections while parallelly writing compressed binary data to the storage.
2. **Time Synchronization**: test the integrity of timestamp and data as potential time inaccurate issue can arise from the distributed nature of cloud computing at both exchange and client sides. Inaccurate timestamp will cause the deviation of backtest results from production and bad decisions of real time trading.
3. **Websocket Latency**: the exchange side might use load balancers to distribute incoming connections across multiple backend instances. Different instances might have different loads, or even physical locations , which can affect the speed at which messages are updated. Thus, it's important to get the fastest connections among others.

The related researches are continuouly conducted by students at each semester and we will publishing all research findings to share knowledge with the wider financial technology and cloud computing community. HFT on the cloud is an area of growing interest and importance.

## Request for AWS Support

To undertake this research project, we are seeking support from AWS in the form of access to AWS resources for testing. Specifically, we are requesting:

- AWS free unlimited plan for the duration of the project to cover usage costs

- Access to bare metal instances, including suitable instance types for HFT testing like m5d.metal, c5d.metal, z1d.metal etc.

- Technical guidance from AWS solution architects and experts in financial services, low-latency applications and EC2 bare metal instances

Access to these resources will allow us to conduct the necessary testing and complete the research project. We believe this project will benefit the broader financial community and cloud computing community by providing valuable insights and guidance on optimizing HFT deployments on AWS. It will also showcase AWS' capabilities in supporting mission-critical, low-latency financial applications.

We would be grateful for AWS' support of this project and the opportunity to work together to advance knowledge in this important area. Please let us know if you have any questions or require further details about the project. We look forward to discussing this opportunity further.