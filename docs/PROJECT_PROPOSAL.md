# Low Latency Tuning for Tick to Trade on AWS Instances

## Team Member

- Zean Huang (graduating December 2024, seeking full-time software engineer roles after graduation)

## Project Overview

This project aims to minimize the latency of tick to trade on AWS instances by applying low latency tuning technologies such as NUMA, isolating CPU cores, and IRQ process. The project will be implemented using C++ and Python, and the testing environment will be deployed on AWS instances and lab machines using Ansible, Vagrant, and Terraform.

## Technologies and Tools

- Low latency tuning technologies: NUMA, isolating CPU cores, IRQ process
- Programming languages: C++, Python
- Hardware: Bare metal AWS instances, VMs hosted on lab machines
- CI/CD and deployment: Ansible (provisioning and automated testing), Vagrant, Terraform

## Hardware Requirements

- Bare metal AWS instances
- VMs hosted on lab machines

## User Interface

This project does not have a user interface component.

## Project Timeline

- March 15th:
- Finish the fake data feed server
- Complete the client
- Implement the server to take order call
- March 25th:
- Propose all potential tuning techniques
- April 1st:
- Deploy servers on lab machines
- Test all proposed tuning methods on lab machines
- April 15th:
- Implement tuning techniques on AWS instances
- Deploy tuning techniques on AWS instances
- May 1st:
- Generate all reports

## Project Goals

1. Bare minimum: Successfully implement and test at least one low latency tuning technique on lab machines
2. Expected completion: Implement and test multiple tuning techniques on both lab machines and AWS instances, providing a comprehensive report on their performance
3. Reach goals: Optimize the implemented tuning techniques to achieve the lowest possible latency on AWS instances

## Individual Contributions

Zean Huang will be solely responsible for all aspects of the project, including:

- Designing and implementing the fake data feed server, client, and order server
- Researching and proposing potential tuning techniques
- Deploying and testing tuning techniques on lab machines and AWS instances
- Generating project reports

## External Resources

- Access to bare metal AWS instances
- Access to lab machines for testing purposes

## Final Deliverables

A comprehensive report detailing the performance of all tested tuning techniques on bare metal AWS instances, including their pros and cons.
