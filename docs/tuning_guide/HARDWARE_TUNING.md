# Hardware Tuning Techniques

These tuning techniqus are related to tuning specific hardware component to lower latency. Therefore they can only be applied to servers to which we have bare metal access.(Only on AWS bare metal `EC2` machines)

- [Hardware Tuning Techniques](#hardware-tuning-techniques)
  - [NIC/Ethernet Adaptor Tuning](#nicethernet-adaptor-tuning)
    - [Diable IRQ Coalescing](#diable-irq-coalescing)
    - [Checksum Offloading](#checksum-offloading)
    - [Other features Offload](#other-features-offload)
    - [Adjust Ring Buffer Size](#adjust-ring-buffer-size)
    - [Quality of Service(QoS)](#quality-of-serviceqos)
    - [Flow Control](#flow-control)
    - [Receive-Side Scaling (RSS)](#receive-side-scaling-rss)
  - [BIOS/UEFI Settings](#biosuefi-settings)
    - [Diable C-States and P-States](#diable-c-states-and-p-states)
    - [Enable High Precision Event Timer (HPET)](#enable-high-precision-event-timer-hpet)
  - [PCIe Settings](#pcie-settings)
    - [PCIe Active State Power Management (ASPM)](#pcie-active-state-power-management-aspm)
    - [MSI (Message Signaled Interrupts)](#msi-message-signaled-interrupts)
  - [I/O Scheduler in Storage Devices](#io-scheduler-in-storage-devices)

## NIC/Ethernet Adaptor Tuning

Network Interface Card (NIC) tuning on Linux is a crucial aspect of optimizing network performance, especially in low-latency applications.

### Diable IRQ Coalescing

Interrupt coalescing is a mechanism used in network interface cards (NICs) to reduce the number of interrupts generated by the hardware. Instead of generating an interrupt for each individual packet that arrives or is sent, the NIC will wait until a certain number of packets have accumulated or a certain amount of time has passed before generating an interrupt. And it's usually divided into two categories:

- **Packet Coalescing:** The NIC accumulates a batch of packets before issuing an interrupt to the processor.
- **Time Coalescing:** The NIC waits for a specified time interval to accumulate packets before generating an interrupt.

Disabling IRQ coalescing (or reducing the coalescing timer to zero) means the NIC will generate an interrupt for each packet immediately upon its arrival or transmission, instead of waiting for more packets to accumulate or for a timer to expire.

An example to do this:

```bash
ethtool -C eth0 rx-usecs 0
```

This command sets the interrupt coalescing timer to 0 microseconds for the receive (rx) queue on `eth0`, making the system process packets as soon as they arrive.

### Checksum Offloading

Disabling checksum offloading can reduce latency for small packets or in environments where the CPU is not a bottleneck.

An example of Disable TCP, UDP, and SCTP checksum offloading:

```bash
ethtool -K eth0 tx off rx off
```

Replace `eth0` with your network interface name. This command turns off both transmit and receive checksum offloading.

### Other features Offload

Offloading certain tasks to the NIC can free up CPU resources, but some offload features might increase latency. You can enable or disable these features based on your needs.

For low latency tuning, **TCP Segmentation Offload (TSO)**, **Generic Segmentation Offload (GSO)**, and **Large Receive Offload (LRO)** should be carefully managed or disabled for low-latency networking, as they can cause delays in packet processing.

e.g.

```bash
ethtool -K eth0 tso off gso off lro off
```

### Adjust Ring Buffer Size

Increasing the ring buffer size can prevent packet loss during traffic spikes, but setting it too high might increase latency. Conversely, smaller buffers can reduce latency but risk packet loss under high load.

For example:

```bash
ethtool -G eth0 rx 512 tx 512
```

This command sets the receive (rx) and transmit (tx) ring buffer sizes to 512 on `eth0`.

### Quality of Service(QoS)

QoS can prioritize network traffic to reduce latency for critical applications.

This is an example where traffic on HTTP port 80 is given high priority. Adjust the match rules to prioritize the traffic relevant to your low-latency requirements using `tc` command(traffic control).

```bash
tc qdisc add dev eth0 root handle 1: prio
tc filter add dev eth0 protocol ip parent 1: prio 1 u32 match ip dport 80 0xffff flowid 1:1
```

### Flow Control

Flow control prevents packet loss during high traffic bursts, but it can introduce latency.

This is an example of how to disables receive and transmit flow control. However, be cautious with this setting, as disabling flow control can lead to packet drops if the network is congested.

```bash
ethtool -A eth0 rx off tx off
```

> below are minor features for NIC that impact low latency and we can potential try tuning it to see how much it helps with low latency applications

### Receive-Side Scaling (RSS)

RSS distributes incoming network packets across multiple CPU cores to prevent a single core from being overloaded.

RSS is typically enabled by default, but the specific configuration can depend on the network driver and hardware. Check the current RSS settings using:

```bash
ethtool -x eth0
```

## BIOS/UEFI Settings

These are the settings on the hardware or firmware level that we can tune to help us reduce latency

### Diable C-States and P-States

In the BIOS/UEFI settings, disabling CPU C-States (idle states) and P-States (performance states) can reduce latency, as the CPU will not enter lower power states.

- **C-States (Idle States):** Control the sleep levels of the CPU. Disabling or reducing the use of deeper C-States keeps the CPU more active, reducing the wake-up latency but increasing power consumption.

- **P-States (Performance States):** Control the operating frequency and voltage of the CPU. Disabling P-State adjustments can lock the CPU at a high performance level, reducing latency due to frequency scaling.

> Changes to these settings typically require a reboot, as they are applied during the system boot process.

### Enable High Precision Event Timer (HPET)

Ensuring that HPET is enabled in the BIOS/UEFI can provide more accurate timer facilities for the OS, improving timing precision.

## PCIe Settings

### PCIe Active State Power Management (ASPM)

Disabling ASPM can reduce latency for PCIe devices but will increase power consumption.

```bash
echo performance | sudo tee /sys/module/pcie_aspm/parameters/policy
```

### MSI (Message Signaled Interrupts)

Ensure that MSI is enabled for devices that support it. MSI can reduce interrupt latency and improve performance over legacy pin-based interrupts.

## I/O Scheduler in Storage Devices

For disks, using the `noop` or `deadline` I/O scheduler can reduce latency by minimizing the overhead of I/O operations.

```bash
echo noop | sudo tee /sys/block/sda/queue/scheduler
```

> Replace `sda` with your device identifier.
