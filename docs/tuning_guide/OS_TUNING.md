# OS Level Tuning Techniques

These are operating system level tuning techniques that we will try to test on AWS instances, which means that these techniques are applicable to any kind of machines.

- [OS Level Tuning Techniques](#os-level-tuning-techniques)
  - [TCP Tuning](#tcp-tuning)
    - [Diable Nagle](#diable-nagle)
    - [Receive and Send buffers](#receive-and-send-buffers)
    - [Quick Acknowledgement](#quick-acknowledgement)
    - [Fast Open(Handshake faster)](#fast-openhandshake-faster)
    - [Keep Alive](#keep-alive)
  - [Isolating CPU Core](#isolating-cpu-core)
    - [Steps to Isolate a CPU](#steps-to-isolate-a-cpu)
    - [Assigning a Process to an Isolated Core](#assigning-a-process-to-an-isolated-core)
  - [IRQ Processing](#irq-processing)
    - [How IRQ Works](#how-irq-works)
    - [Commands that manage IRQ on Linux](#commands-that-manage-irq-on-linux)
    - [Configuring IRQ Affinity](#configuring-irq-affinity)
  - [Busy Polling](#busy-polling)
    - [How it helps with low latency tuning](#how-it-helps-with-low-latency-tuning)
    - [Considerations when using Busy Polling](#considerations-when-using-busy-polling)
  - [CPU Power Saving Mode(To check if applicable to AWS)](#cpu-power-saving-modeto-check-if-applicable-to-aws)
  - [Memory Settings](#memory-settings)
    - [Disable Transparent Huge Pages (THP)](#disable-transparent-huge-pages-thp)
    - [Tuning Memory Swappiness](#tuning-memory-swappiness)
  - [Python Tuning](#python-tuning)
    - [Switch to Pypy](#switch-to-pypy)
    - [Use C-Optimized SSL Library](#use-c-optimized-ssl-library)
    - [Configuring Specific SSL Ciphers](#configuring-specific-ssl-ciphers)
    - [Memory Management](#memory-management)
  - [Kernal Bypass](#kernal-bypass)
    - [Solarflare OpenOnload](#solarflare-openonload)
    - [DPDK (Data Plane Development Kit)](#dpdk-data-plane-development-kit)
    - [RDMA (Remote Direct Memory Access)](#rdma-remote-direct-memory-access)
    - [AF_XDP(Linux Native)](#af_xdplinux-native)
    - [Differences between using DPDK and AF_XDP](#differences-between-using-dpdk-and-af_xdp)

## TCP Tuning

### Diable Nagle

Nagle's algorithm is used in TCP/IP networks to reduce the number of packets that need to be sent over the network. It combines a number of small outgoing messages and sends them all at once to increase the efficiency of the network. However, this can introduce a delay.

Disabling Nagle's algorithm tells the network layer to send data immediately, even if only a small amount of data is ready to be sent, effectively bypassing Nagle's algorithm for that connection. So it can be used to improve the latency in network communications(in this case, minimize the time for order to be sent out).

This is usually achieved by setting `TCP_NODELAY` flag on a socket:

```python
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('hostname', port))
sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
```

### Receive and Send buffers

`SO_RCVBUF` adn `SO_SNDBUF` options set the size of the receive and send buffers, respectively. Adjusting these buffer sizes can affect throughput and latency. Smaller buffers can reduce latency by sending data more frequently, but they might also reduce throughput.

### Quick Acknowledgement

Enabling `TCP_QUICKACK` allows the TCP stack to send acknowledgments (ACKs) immediately, rather than delaying them. This can reduce latency, especially in protocols where each packet requires an acknowledgment before the next one is sent.

### Fast Open(Handshake faster)

`TCP_FASTOPEN` option reduces the latency incurred during the TCP three-way handshake by allowing data to be sent during the initial SYN packet, reducing the time to establish a TCP connection.

### Keep Alive

**Different From Websocket connection keep alive. Performance differences?**

`TCP_KEEPALIVE` option is used to send keepalive probes to check if a connection is still alive. Adjusting keepalive intervals can help detect dead connections faster, but it's more about maintaining the connection efficiently than reducing latency.

## Isolating CPU Core

Isolating CPU cores involves dedicating specific cores to a particular task or set of tasks, preventing other processes from executing on these cores. This can be beneficial for low-latency applications because it reduces context switching, CPU contention, and potential performance interference from other processes.

You can run the `lscpu` command on Linux to check the number of CPU cores as well as get detailed information abou the CPU architecture.

```bash
lscpu
```

### Steps to Isolate a CPU

1. Edit the GRUB configuration file. For most Linux distributions, this file is located at `/etc/default/grub`.

2. Add or modify the `isolcpus` parameter in the `GRUB_CMDLINE_LINUX_DEFAULT` line. For example, to isolate core 2, you would add `isolcpus=2`:

```bash
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash isolcpus=2"
```

> `quiet`: This option reduces the amount of messages output to the screen during the boot process. Instead of showing detailed kernel messages, the system displays a minimal amount of information. This makes the boot process look cleaner and less cluttered, which can be desirable on desktop systems where a streamlined user experience is preferred.
>
> `splash`: This option enables a splash screen, which is a graphical representation (like a logo or an image) displayed during the boot process. The splash screen is often used in conjunction with a progress bar or animations to provide a visual indicator of the boot process without displaying the underlying boot messages.

3. Update GRUB and reboot the machine:

```bash
sudo update-grub
sudo reboot
```

Note: What a default GRUB configuration file looks like on Ubuntu 22.04 LTS

```bash
# If you change this file, run 'update-grub' afterwards to update
# /boot/grub/grub.cfg.
# For full documentation of the options in this file, see:
#   info -f grub -n 'Simple configuration'

GRUB_DEFAULT=0
GRUB_TIMEOUT_STYLE=hidden
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX=""

# Uncomment to enable BadRAM filtering, modify to suit your needs
# This works with Linux (no patch required) and with any kernel that obtains
# the memory map information from GRUB (GNU Mach, kernel of FreeBSD ...)
#GRUB_BADRAM="0x01234567,0xfefefefe,0x89abcdef,0xefefefef"

# Uncomment to disable graphical terminal (grub-pc only)
#GRUB_TERMINAL=console

# The resolution used on graphical terminal
# note that you can use only modes which your graphic card supports via VBE
# you can see them in real GRUB with the command `vbeinfo'
#GRUB_GFXMODE=640x480

# Uncomment if you don't want GRUB to pass "root=UUID=xxx" parameter to Linux
#GRUB_DISABLE_LINUX_UUID=true

# Uncomment to disable generation of recovery mode menu entries
#GRUB_DISABLE_RECOVERY="true"

# Uncomment to get a beep at grub start
#GRUB_INIT_TUNE="480 440 1"
```

### Assigning a Process to an Isolated Core

To assign a process to a specific core, you can use the `taskset` command. For example, to run a process on core 2, you would use:

```bash
taskset -c 2 <command>
```

## IRQ Processing

IRQ (Interrupt Request) processing is a mechanism used by computer hardware and operating systems to handle requests for attention from hardware devices. It is a core component of a computer's architecture that allows it to respond to events from hardware devices like disk controllers, network interfaces, and other peripherals.

### How IRQ Works

1. **Interrupt Generation:** When a hardware device needs the CPU's attention (e.g., when new data arrives at a network card), it sends an interrupt signal to the CPU.
2. **Interrupt Detection:** The CPU constantly checks for these signals and, upon detecting an interrupt, temporarily halts the current execution thread.
3. **Interrupt Handling:** The CPU then calls a specific function, known as an interrupt handler or interrupt service routine (ISR), which is designated to process the interrupt.
4. **Task Prioritization:** The interrupt handler processes the interrupt, performs the necessary actions (such as reading data from the device), and then resumes the interrupted task or schedules tasks as per their priority.

### Commands that manage IRQ on Linux

On Linux, IRQs are managed and handled by the kernel. You can interact with IRQ settings and distributions using several tools and files:

- `/proc/interrupts`: This file shows the number of interrupts per CPU per I/O device. It helps in understanding how IRQs are distributed across CPUs.
  - to monitor the interrupts in real time

```
watch -n 1 cat /proc/interrupts
```

- `irqbalance`: This is a daemon designed to distribute hardware interrupts across processors on a multiprocessor system, aiming to improve performance.
- `taskset` or `isolcpus`: These tools can be used to set CPU affinity for specific processes or to isolate CPUs for handling specific tasks, respectively, which can help in reducing latency for critical tasks.

### Configuring IRQ Affinity

To assign a specific IRQ to a particular CPU or CPU core to reduce latency and improve performance:

1. Determine the IRQ number you want to manage. View the `/proc/interrupts` file to see existing IRQ assignments.
2. Use the `/proc/irq/default_smp_affinity` file to set the default IRQ affinity or `/proc/irq/{IRQ_NUMBER}/smp_affinity` to set the affinity for a specific IRQ.
3. Write the CPU mask to the `smp_affinity` file. For example, to assign IRQ number 10 to CPU 0, you would write `1` to `/proc/irq/10/smp_affinity`.

> NOTE: The smp_affinity file uses a bitmask to indicate to which CPUs the interrupt can be delivered. This bitmask is a hexadecimal value that maps to the binary representation where each bit represents a CPU core. Here's how it works:
>
> - Each bit in the mask corresponds to a CPU core, with the least significant bit (the rightmost bit) corresponding to CPU0.
> - If a bit is set to 1, the corresponding CPU is eligible to handle the interrupt.
> - To assign an interrupt to a specific CPU, you would set the bit corresponding to that CPU to 1 and all other bits to 0.

## Busy Polling

Busy polling, also known as spin-waiting, in the context of Linux, involves continuously checking a condition in a tight loop, without relinquishing the CPU. This technique can be used for scenarios where you expect a change in condition imminently and want to react to it as quickly as possible, hence achieving low latency.

Example of a direct polling loop:

```C
while (!condition_met()) {
    // Optionally, perform a minimal delay or yield operation
}
process_event();
```

As for minimizing tick to trade, one way that we can use busy polling to minimize latency is we can use system calls such as `select` or `poll` with zero or very low timeout to create a busy polling loop:

```C
struct pollfd fds[1];
fds[0].fd = socket_fd;
fds[0].events = POLLIN;

while (!done) {
    int ret = poll(fds, 1, 0);  // 0 timeout for busy polling
    if (ret > 0) {
        // Handle I/O event
    }
    // Otherwise, immediately loop back
}
```

### How it helps with low latency tuning

1. **Immediate Response:** Busy polling allows a program to detect and respond to an event the instant it occurs, minimizing the reaction time.
2. **Avoiding Context Switches:** By not yielding the CPU (as you would with blocking I/O calls or sleep operations), busy polling avoids context switches, which can introduce latency.
3. **Priority to Critical Tasks:** In low-latency systems, such as those used in high-frequency trading, busy polling ensures that critical tasks have immediate access to the CPU and can respond to events faster than if they had to wait for a scheduler's intervention.

### Considerations when using Busy Polling

1. **CPU Utilization:** Busy polling can lead to high CPU utilization because the CPU is constantly working, checking the condition in the loop.
2. **Power Consumption**
3. **Heat Generation**
4. **Impact on Multitasking:** On systems with limited CPU resources, busy polling can affect the performance of other tasks.

## CPU Power Saving Mode(To check if applicable to AWS)

To change CPU power saving modes to reduce latency, you can modify the CPU governor settings. The CPU governor controls the scaling of the CPU frequency.

1. Check the current CPU governor setting

```bash
cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
```

2. Change the governor to `performance`

```bash
echo performance | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
```

This change will apply immediately but will not persist across reboots. To make it permanent, you can set this governor in your system’s startup scripts or use a tool like `cpupower` or `cpufreq-set`.

## Memory Settings

### Disable Transparent Huge Pages (THP)

Huge pages can increase memory performance by reducing page table overhead, but they may introduce latency due to memory allocation delays.

```bash
echo never | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
```

### Tuning Memory Swappiness

The `vm.swappiness` parameter controls the tendency of the kernel to swap memory to disk. Reducing this value can decrease disk swapping, which is beneficial for latency-sensitive applications.

```bash
sysctl vm.swappiness=10
```

## Python Tuning

### Switch to Pypy

**What’s PyPy?** PyPy is an alternative Python interpreter that includes a Just-In-Time (JIT) compiler, which can significantly improve the execution speed of Python code.

**Difference from CPython:** The standard Python interpreter (CPython) compiles Python code to bytecode which is then interpreted. PyPy, on the other hand, compiles Python code to bytecode and then further to machine code at runtime, using JIT compilation, which can lead to substantial performance improvements, especially for long-running processes.

Steps to Switch to PyPy:

1. Download and install PyPy from its official website
2. Use the `pypy` command instead of `python` to execute your scripts, e.g., `pypy myscript.py`.

### Use C-Optimized SSL Library

Python's `ssl` module, particularly in CPython, can use OpenSSL for SSL/TLS operations, which is already C-optimized. Ensuring you're using the latest version of OpenSSL can enhance performance.

### Configuring Specific SSL Ciphers

SSL/TLS ciphers can impact the handshake and data transfer performance. Choosing ciphers that prioritize speed and efficiency can reduce latency.

Steps to configure:

1. **Identify low-latency ciphers:** Research and select ciphers that offer a balance of security and performance. AES-GCM, for instance, is known for its performance.
2. **Configure Python to use these ciphers:** When creating an SSLContext, you can specify the cipher suite using the set_ciphers method.

```Python
import ssl

context = ssl.create_default_context()
context.set_ciphers('ECDHE-RSA-AES128-GCM-SHA256')
```

### Memory Management

Minimize memory allocations in critical code paths to reduce garbage collection overhead. This can be achieved by reusing buffers and objects instead of creating new ones frequently.

## Kernal Bypass

> NOTE: Most of the kernal bypass techniques does not apply to AWS instances

### Solarflare OpenOnload

OpenOnload is a high-performance network stack from Solarflare that can bypass the kernel's TCP/IP stack to reduce latency and improve throughput for networking applications.

> **Link Referenced:** https://blog.cloudflare.com/kernel-bypass

### DPDK (Data Plane Development Kit)

DPDK is a set of libraries and drivers for fast packet processing. It provides a framework for developing high-speed data packet networking applications. DPDK works by allocating hugepages in memory and using polling drivers for NICs to bypass the kernel's networking stack, which reduces the overhead and latency.

### RDMA (Remote Direct Memory Access)

RDMA allows the exchange of data directly between the memory of two computers without involving the CPU, cache, or operating system of either system. This direct memory-to-memory data transfer occurs at high speeds with low latency.

### AF_XDP(Linux Native)

**What is XDP?** XDP(eXpress Data Path) is a high-performance, programmable network data path that allows packet processing at the earliest point in the Linux networking stack, right after a packet is received by the NIC.

**AF_XDP(Address Family XDP)** is a socket interface in the Linux kernel that allows user-space applications to directly access the high-performance data path provided by XDP. It is part of the XDP ecosystem and leverages XDP's fast packet processing capabilities.

With `AF_XDP`, applications can communicate directly with the NIC's memory buffers via a socket interface, bypassing much of the kernel's networking stack and reducing overhead. It extends XDP functionality into user space, allowing for extremely fast packet processing in applications.

> AF_XDP is supported on ENA
>
> Referenced Link: https://github.com/amzn/amzn-drivers/blob/master/kernel/linux/ena/ENA_Linux_Best_Practices.rst
>
> Under the question: Q: What are the optimal settings for achieving the best latency

### Differences between using DPDK and AF_XDP

- **Scope:**
  - DPDK is a comprehensive set of libraries and drivers designed to accelerate packet processing on a wide range of CPU architectures. It provides a rich ecosystem of networking functions, including memory management, queueing, and buffering.
  - AF_XDP is integrated into the Linux kernel, providing a mechanism to achieve high-speed packet processing while leveraging existing kernel infrastructure. It allows applications to use a socket-like API for direct packet transmission and reception.
- **Integration Level:**
  - DPDK provides a comprehensive development framework for high-speed networking and requires explicit integration into applications
  - AF_XDP offers a more straightforward approach to achieving high performance by extending the socket API in Linux.
- **Ease of Use:**
  - AF_XDP tends to be easier to use and integrate into existing applications compared to DPDK, which requires more in-depth development and system configuration.
- **Performance:**
  - DPDK often delivers higher performance and lower latencies due to its more extensive optimization and bypass capabilities
  - AF_XDP has made significant strides in performance and can be sufficient for many high-performance networking needs without the complexity of DPDK.

> Reference Link:
>
> First paragraph in
> https://developers.redhat.com/blog/2018/12/06/achieving-high-performance-low-latency-networking-with-xdp-part-1#xdp__from_zero_to_14_mpps
