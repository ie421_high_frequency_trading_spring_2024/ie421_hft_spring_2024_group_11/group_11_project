import websocket
# import ssl
import sys

try:
    ws = websocket.WebSocket()
    # ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    # ws.connect("wss://localhost:6789", sslopt={"cert_reqs": ssl.CERT_NONE})   
    ws.connect("ws://3.145.191.90:6789")   
    result = ws.recv()  # Receive response
    print(result)  # Output the response to stdout
    ws.close()
except Exception as e:
    print(e)
    sys.exit(1)  # Ensure non-zero exit on failure for Ansible to detect
