import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv('../../.env')

# Retrieve environment variables

cli_alias = os.getenv('CLI_ALIAS')
cli_hostname = os.getenv('CLI_HOSTNAME')
cli_user = os.getenv('CLI_USER')
cli_private_key = os.getenv('CLI_PRIVATE_KEY')

ex_alias = os.getenv('EX_ALIAS')
ex_hostname = os.getenv('EX_HOSTNAME')
ex_user = os.getenv('EX_USER')
ex_private_key = os.getenv('EX_PRIVATE_KEY')

# Generate hosts.ini content
hosts_content = f"""
[{cli_alias}]
{cli_hostname} 

[{ex_alias}]
{ex_hostname} 
"""

# Write to hosts.ini file
with open('./infra/inventory/hosts.ini', 'w') as hosts_file:
    hosts_file.write(hosts_content.strip())
