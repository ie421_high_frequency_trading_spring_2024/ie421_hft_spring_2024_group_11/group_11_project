mkdir certificate
openssl req -newkey rsa:2048 -nodes -keyout ./certificate/key.pem -x509 -days 365 -out ./certificate/certificate.pem
