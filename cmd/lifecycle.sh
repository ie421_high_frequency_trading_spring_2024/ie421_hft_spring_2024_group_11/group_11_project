#!/bin/bash

# Check if a parameter was provided
if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 <wait_duration_in_seconds> <frequency>"
  exit 1
fi

# Assign the first parameter to WAIT_DURATION
WAIT_DURATION=$1
FREQ=$2
PCAP_FILEPATH="logs/$(date +%Y-%m-%d_%H-%M-%S).pcap"

# sudo tcpdump -i any -w $pcap_filepath '(src port 6789 or dst port 5000)' &
sudo tcpdump -i lo0 -w $PCAP_FILEPATH &
TCPDUMP_PID=$!
echo "tcpdump started with PID $TCPDUMP_PID"

sleep 1

# Start order API server
echo "Starting Order API Server..."
./scripts/start_order_api_server.sh en &
ORDER_API_PID=$!
echo $ORDER_API_PID

# Start data feed server
echo "Starting Data Feed Server..."
./scripts/start_data_feed_server.sh en $FREQ &
DATA_FEED_PID=$!
echo $DATA_FEED_PID

# Wait a bit for servers to initialize (adjust time as needed)
sleep 3

# Start client
echo "Starting Client..."
./scripts/start_client.sh en &
CLIENT_PID=$!
echo $CLIENT_PID

# Let the client run for the specified duration
echo "Client will run for $WAIT_DURATION seconds."
sleep $WAIT_DURATION

echo "Shutting down Data Feed Server..."
pkill -f 'data_feed/main.py'
# kill -9 $DATA_FEED_PID

# Shutdown client first
echo "Shutting down Client..."
pkill -f 'client/main.py'
# kill -9 $CLIENT_PID

# Then shut down the servers
echo "Shutting down Order API Server..."
pkill -f 'order_api/main.py'
# kill -9 $ORDER_API_PID

# Gracefully terminate tcpdump by sending SIGTERM signal
echo "Stopping tcpdump..."
sudo kill $TCPDUMP_PID

# Wait a bit to ensure tcpdump has time to exit cleanly
sleep 2

echo "All processes have been terminated."
echo "Start analysing..."
python analysis/tick_to_trade.py $PCAP_FILEPATH

