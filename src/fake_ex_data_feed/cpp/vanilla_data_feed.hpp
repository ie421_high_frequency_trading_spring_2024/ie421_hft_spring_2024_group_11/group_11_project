#pragma once

#include "abstract_data_feed.hpp"
#include <string>

class VanillaDataFeedServer : public DataFeedServer {
public:
    VanillaDataFeedServer(const std::string& hostname, int port, double delay_in_seconds);
    void run() override;
    void worker(const std::string& path) override;
    std::string generate_data() override;
    void send_data(const std::string& data) override;

private:
    std::string hostname;
    int port;
    double delay_in_seconds;
    std::string file_path;
};