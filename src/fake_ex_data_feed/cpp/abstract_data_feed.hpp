#pragma once

#include <string>

class DataFeedServer {
public:
    virtual void run() = 0;
    virtual void worker(const std::string& path) = 0;
    virtual std::string generate_data() = 0;
    virtual void send_data(const std::string& data) = 0;
};