#include "vanilla_data_feed.hpp"
#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>

VanillaDataFeedServer::VanillaDataFeedServer(const std::string& hostname, int port, double delay_in_seconds)
    : hostname(hostname), port(port), delay_in_seconds(delay_in_seconds),
      file_path("/Users/wyatt/dev/mps/ie421/sp24_low_latency_proj/src/server/data_feed/BTCUSDT_mock_data") {}

void VanillaDataFeedServer::run() {
    worker("");
}

void VanillaDataFeedServer::worker(const std::string& path) {
    while (true) {
        std::string data = generate_data();
        send_data(data);
        std::this_thread::sleep_for(std::chrono::duration<double>(delay_in_seconds));
    }
}

std::string VanillaDataFeedServer::generate_data() {
    std::ifstream file(file_path);
    std::string line;
    if (std::getline(file, line)) {
        file.seekg(0, std::ios::beg);
        return line;
    }
    return "";
}

void VanillaDataFeedServer::send_data(const std::string& data) {
    // TODO: Implement sending data over websocket
    std::cout << data << std::endl;
}