#pragma once

#include "vanilla_data_feed.hpp"

class EncryptedDataFeedServer : public VanillaDataFeedServer {
public:
    using VanillaDataFeedServer::VanillaDataFeedServer;
    void send_data(const std::string& data) override;
};