#include "vanilla_data_feed.hpp"
#include "encrypted_data_feed.hpp"
#include <iostream>
#include <memory>

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "Usage: ./data_feed_server [vanilla|en] [freq]" << std::endl;
        return 1;
    }

    std::string parameter = argv[1];
    double freq = 1.0 / std::stod(argv[2]);
    std::unique_ptr<DataFeedServer> df_server;

    if (parameter == "vanilla") {
        df_server = std::make_unique<VanillaDataFeedServer>("localhost", 6789, freq);
    } else if (parameter == "en") {
        df_server = std::make_unique<EncryptedDataFeedServer>("localhost", 6789, freq);
    } else {
        std::cout << "Invalid parameter. Use one of the following: vanilla, en" << std::endl;
        return 1;
    }

    df_server->run();

    return 0;
}