from abstract_data_feed import DataFeedServer

import asyncio
import websockets
import json
import random
from datetime import datetime
from uuid import uuid4


class VanillaDataFeedServer(DataFeedServer):
    def __init__(self, hostname, port, delay_in_secconds) -> None:
        self.hostname = hostname
        self.port = port
        self.delay_in_seconds = delay_in_secconds
        self.file_path = "./src/server/data_feed/BTCUSDT_mock_data"

    def create_server(self):
        return websockets.serve(self.worker, self.hostname, self.port)

    def run(self):
        start_server = self.create_server()
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    async def worker(self, ws, path):
        while True:
            await self.send_data(ws)
            await asyncio.sleep(self.delay_in_seconds)

    def get_market_data_line(self, file_path):
        with open(file_path, "r") as file:
            while True:
                file.seek(0)
                for line in file:
                    yield json.loads(line.strip())


    def generate_data(self):
        data_generator = self.get_market_data_line(self.file_path)
        return next(data_generator)

    async def send_data(self, ws):
        await ws.send(json.dumps(self.generate_data()))
