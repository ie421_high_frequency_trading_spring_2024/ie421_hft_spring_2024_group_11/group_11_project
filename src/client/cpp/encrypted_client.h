#ifndef ENCRYPTED_CLIENT_HPP
#define ENCRYPTED_CLIENT_HPP

#include "./vanilla_client.h"

class EncryptedClient : public VanillaClient {
public:
    EncryptedClient(const std::string& df_hostname, const std::string& df_port,
                    const std::string& or_hostname, const std::string& or_port);
    std::string data_feed_url() const override;
    void worker() override;
    std::string order_req_url() const override;
    void send_response(const std::string& msg) override;
};

#endif // ENCRYPTED_CLIENT_HPP