#ifndef ABSTRACT_CLIENT_HPP
#define ABSTRACT_CLIENT_HPP

#include <string>

class Client {
public:
    virtual ~Client() {}
    virtual std::string generate_response(const std::string& msg) = 0;
    virtual void send_response(const std::string& msg) = 0;
    virtual std::string recv_message() = 0;
    virtual void worker() = 0;
    virtual void run() = 0;
    virtual std::string data_feed_url() const = 0;
    virtual std::string order_req_url() const = 0;
};

#endif // ABSTRACT_CLIENT_HPP