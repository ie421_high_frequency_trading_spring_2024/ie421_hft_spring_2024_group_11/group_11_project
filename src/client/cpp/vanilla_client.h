#ifndef VANILLA_CLIENT_HPP
#define VANILLA_CLIENT_HPP

#include "./abstract_client.h"
#include <string>

class VanillaClient : public Client {
protected:
    std::string df_hostname;
    std::string df_port;
    std::string or_hostname;
    std::string or_port;

public:
    VanillaClient(const std::string& df_hostname, const std::string& df_port,
                  const std::string& or_hostname, const std::string& or_port);
    std::string data_feed_url() const override;
    std::string order_req_url() const override;
    std::string generate_response(const std::string& msg) override;
    void send_response(const std::string& msg) override;
    std::string recv_message() override;
    void worker() override;
    void run() override;
};

#endif // VANILLA_CLIENT_HPP