#include "./vanilla_client.h"
#include <iostream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <curl/curl.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

VanillaClient::VanillaClient(const std::string& df_hostname, const std::string& df_port,
                             const std::string& or_hostname, const std::string& or_port)
    : df_hostname(df_hostname), df_port(df_port), or_hostname(or_hostname), or_port(or_port) {
}

std::string VanillaClient::data_feed_url() const {
    return "ws://" + df_hostname + ":" + df_port;
}

std::string VanillaClient::order_req_url() const {
    return "http://" + or_hostname + ":" + or_port + "/order";
}

std::string VanillaClient::generate_response(const std::string& msg) {
    json data = {
        {"type", "buy"},
        {"amount", "100"},
        {"id", json::parse(msg)["id"]},
        {"timestamp", std::chrono::system_clock::now()}
    };
    return "Content-Type: application/json\r\n\r\n" + data.dump();
}

void VanillaClient::send_response(const std::string& msg) {
    CURL* curl = curl_easy_init();
    if (curl) {
        std::string headers = generate_response(msg);

        curl_easy_setopt(curl, CURLOPT_URL, order_req_url().c_str());
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, headers.c_str());

        CURLcode res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
        }

        curl_easy_cleanup(curl);
    }
}

std::string VanillaClient::recv_message() {
    // TODO: Implement WebSocket message receiving
    return "";
}

void VanillaClient::worker() {
    // TODO: Implement WebSocket connection and message handling
}

void VanillaClient::run() {
    worker();
}