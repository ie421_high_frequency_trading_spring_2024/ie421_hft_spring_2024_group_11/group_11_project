#include <iostream>
#include <memory>
#include "./vanilla_client.h"
#include "./encrypted_client.h"

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cout << "Usage: ./client [vanilla|en]" << std::endl;
        return 1;
    }

    std::string parameter = argv[1];
    std::unique_ptr<Client> client;

    if (parameter == "vanilla") {
        client = std::make_unique<VanillaClient>("localhost", "6789", "127.0.0.1", "5000");
    } else if (parameter == "en") {
        client = std::make_unique<EncryptedClient>("localhost", "6789", "127.0.0.1", "5000");
    } else {
        std::cout << "Invalid parameter. Use one of the following: vanilla, en" << std::endl;
        return 1;
    }

    client->run();

    return 0;
}