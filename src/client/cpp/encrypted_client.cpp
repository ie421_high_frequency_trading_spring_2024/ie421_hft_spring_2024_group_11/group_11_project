#include "./encrypted_client.h"
#include <iostream>
#include <curl/curl.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

EncryptedClient::EncryptedClient(const std::string& df_hostname, const std::string& df_port,
                                 const std::string& or_hostname, const std::string& or_port)
    : VanillaClient(df_hostname, df_port, or_hostname, or_port) {
    // Disable SSL verification
    curl_global_init(CURL_GLOBAL_DEFAULT);
}

std::string EncryptedClient::data_feed_url() const {
    return "wss://" + df_hostname + ":" + df_port;
}

void EncryptedClient::worker() {
    // TODO: Implement WebSocket connection and message handling
    // Disable SSL verification for the WebSocket connection
}

std::string EncryptedClient::order_req_url() const {
    return "https://" + or_hostname + ":" + or_port + "/order";
}

void EncryptedClient::send_response(const std::string& msg) {
    CURL* curl = curl_easy_init();
    if (curl) {
        std::string headers = generate_response(msg);
        json data = json::parse(msg);

        curl_easy_setopt(curl, CURLOPT_URL, order_req_url().c_str());
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.dump().c_str());
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers.c_str());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

        CURLcode res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
        }

        curl_easy_cleanup(curl);
    }
}