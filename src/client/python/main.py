
from vanilla_client import VanillaClient
from encrypted_client import EncryptedClient
from encrypted_data_feed_client import EncryptedDataFeedClient
from encrypted_order_api_client import EncryptedOrderAPIClient
from dotenv import load_dotenv
import os
import sys
import sslkeylog
import ssl

load_dotenv()
print(os.getenv("SSLKEYLOGFILE"))
print(ssl.OPENSSL_VERSION)
sslkeylog.set_keylog(os.environ.get('SSLKEYLOGFILE')) 

if len(sys.argv) != 3:
    print("Usage: python script.py [vanilla|df_en|oa_en|en] [server_ip]")
    sys.exit(1)  # Exit the script if no parameter or too many parameters are provided

encrypt = sys.argv[1]  # Get the parameter
ip_addr = sys.argv[2]  # Get the parameter
client = None

if encrypt == "vanilla":
    client = VanillaClient(ip_addr, "6789",ip_addr, "5000")
elif encrypt == "df_en":
    client = EncryptedDataFeedClient(ip_addr, "6789",ip_addr, "5000")
elif encrypt == "oa_en":
    client = EncryptedOrderAPIClient(ip_addr, "6789",ip_addr, "5000")
elif encrypt == "en":
    client = EncryptedClient(ip_addr, "6789", ip_addr, "5000")
else:
    print("Invalid parameter. Use one of the following: vanilla, df_en, oa_en, en")
    sys.exit(1)

client.run()