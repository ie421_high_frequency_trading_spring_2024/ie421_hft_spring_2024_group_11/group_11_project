import time

def place_single_order(self, clientorderid, price, qty, side, order_type, tif):
        current_time = int(time.time() * 1000)
        full_url = self.base_url + "/order?"

        query_string = f"newClientOrderId={clientorderid}&symbol={self.coin}&side={side}&type={order_type}&timeInForce={tif}&price={price}&quantity={qty}&timestamp={current_time}"

        signature = self.get_sign(query_string)
        query_string = query_string + f"&signature={signature}"

        res = self.session.post(full_url + query_string, headers=self.headers)

        return res.json()